<?php

    require_once "../database/dbConnection.php";

    if($_SERVER['REQUEST_METHOD'] == "GET") {
        if(isset($_GET['url'])) {

            $id = uniqid();
            $url = $_GET['url'];

            if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {  // filtro il parametro, verifico se è un URL
                // RETURN JSON ERROR
                $error = json_encode("Non hai inserito un URL valido, aggiungi il protocollo HTTP(S).");
                echo $error;
                die();
            }

            $stmt = dbConnection::getIstance()->getDb()->prepare("INSERT INTO links(OriginalUrl, Shortcut, FK_UserId) VALUES(?, ?, null)");
            $stmt->bind_param("ss", $url, $id);
            $stmt->execute();
            $stmt->close();

            $link = new \stdClass(); // oggetto vuoto: per evitare il warning di php
            $link->url = "http://localhost:8888/$id";
            $jsonResult = json_encode($link);
            echo $jsonResult;
        }
    }

?>