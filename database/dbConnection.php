<?php

class dbConnection
{
    private $connection;
    static private $istance = NULL;

    static function getIstance(){
        if(self::$istance == NULL) {
            self::$istance = new dbConnection();
        }
        return self::$istance;
    }

    private function __construct()
    {
        $this->connection = new mysqli("localhost", "root", "root", "tireindirizzo");
    }

    private function __clone() { }

    function getDb() {
        return $this->connection;
    }
}