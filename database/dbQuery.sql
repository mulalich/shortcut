
CREATE DATABASE tireindirizzo;

use tireindirizzo;

CREATE TABLE users(
	UserId INT NOT NULL AUTO_INCREMENT,
	Nickname varchar(25) NOT NULL,
	Password varchar(65) NOT NULL,
    PRIMARY KEY (UserId)
);

CREATE TABLE links(
	LinkId INT NOT NULL AUTO_INCREMENT,
	OriginalUrl varchar(1000) NOT NULL,
	Shortcut varchar(100) NOT NULL,
    FK_UserId INT,
    PRIMARY KEY (LinkId),
	FOREIGN KEY (FK_UserId) REFERENCES users(UserId)
);

CREATE TABLE stats(
	StatId INT NOT NULL AUTO_INCREMENT,
	ClickDate date,
    FK_LinkId INT,
    PRIMARY KEY (StatId),
	FOREIGN KEY (FK_LinkId) REFERENCES links(LinkId)
);
