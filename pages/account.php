<?php
    session_start();
    require_once "../models/user.php";
    require_once "../database/dbConnection.php";
    if(!isset($_SESSION['user'])) { header("Location: ../home.php"); }
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>TiReindirizzo - Account</title>
  <!-- Favicon -->
  <link href="../assets/img/favicon.png" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="../assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
  <link href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="../assets/css/argon.css?v=1.0.0" rel="stylesheet">

    <style>
        body {
            background: linear-gradient(87deg, #5e72e4 0, #825ee4 100%) !important;
        }
    </style>
</head>

<body>
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="../home.php">Crea un nuovo shortcut</a>
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold">
                      <?php
                        $user = unserialize($_SESSION['user']);
                        echo $user->nickname;
                      ?>
                  </span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <a href="#!" class="dropdown-item">
                <?php
                    echo "<a href='?logout=1'>Logout</a>";
                    if(isset($_GET['logout'])) {
                        unset($_SESSION['user']);
                        echo "<script type='text/javascript'>location.href = '../home.php';</script>";
                    }
                ?>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
      <div class="row">
        <div class="col-xl-4">
          <div class="card shadow">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">I tuoi link</h3>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <!-- Projects table -->
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                <tr>
                  <th scope="col">URL</th>
                  <th scope="col">Visitatori</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $userId = unserialize($_SESSION['user'])->userId;
                $links = dbConnection::getIstance()->getDb()->query("SELECT * FROM links WHERE FK_UserId = '$userId'");
                while ($row = $links->fetch_array(MYSQLI_NUM)) {
                    $clicks = dbConnection::getIstance()->getDb()->query("SELECT * FROM stats WHERE FK_LinkId = '$row[0]'")->num_rows;
                    echo "<tr>";
                    echo "<th scope='row'><a href='http://localhost:8888/$row[2]' target='_blank'>$row[2]</a></th> ";
                    echo "<td>$clicks</td>";
                    echo "<tr>";
                }
                ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-xl-8 mb-5 mb-xl-0">
            <div class="row">
                <div class="col-xl-6 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">I tuoi click</h5>
                                    <?php
                                    $userId = unserialize($_SESSION['user'])->userId;
                                    $myLinks = dbConnection::getIstance()->getDb()->query("SELECT LinkId FROM links WHERE FK_UserId = $userId");
                                    $myClicks = array();
                                    while($row = $myLinks->fetch_array(MYSQLI_NUM)){
                                        array_push($myClicks, $row[0]);
                                    }
                                    $totalClicks = 0;
                                    for ($i = 0; $i < count($myClicks); $i++) {
                                        $totalClicks = $totalClicks + dbConnection::getIstance()->getDb()->query("SELECT * FROM stats WHERE FK_LinkId = $myClicks[$i]")->num_rows;
                                    }
                                    echo "<span class='h2 font-weight-bold mb-0'>$totalClicks</span>"
                                    ?>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                        <i class="fas fa-chart-bar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Click globali</h5>
                                    <?php
                                        $globalClicks = dbConnection::getIstance()->getDb()->query("SELECT * FROM stats")->num_rows;
                                        echo "<span class='h2 font-weight-bold mb-0'>$globalClicks</span>";
                                    ?>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                                        <i class="fas fa-chart-pie"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 50px;">
                <div class="col-xl-6 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Percentuale clicks</h5>
                                    <?php
                                        if($globalClicks == 0) { $globalClicks = 1; }
                                        $perc = round($totalClicks / $globalClicks * 100, 2);
                                        echo "<span class='h2 font-weight-bold mb-0'>$perc %</span>";
                                    ?>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                                        <i class="fas fa-percent"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Utenti registrati</h5>
                                    <?php
                                        $totalUsers = dbConnection::getIstance()->getDb()->query("SELECT * FROM users")->num_rows;
                                        echo "<span class='h2 font-weight-bold mb-0'>$totalUsers</span>";
                                    ?>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                                        <i class="fas fa-users"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <!-- Optional JS -->
  <script src="../assets/vendor/chart.js/dist/Chart.min.js"></script>
  <script src="../assets/vendor/chart.js/dist/Chart.extension.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.0.0"></script>
</body>

</html>