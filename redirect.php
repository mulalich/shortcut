<?php
    require_once "database/dbConnection.php";
    $conn = dbConnection::getIstance()->getDb();

    $code = $_GET['param'];
    $result = $conn->query("SELECT * FROM links WHERE Shortcut = '$code'");

    if($result->num_rows == 1) {
        $row = $result->fetch_array(MYSQLI_NUM);

        $date = date('Y-m-d');
        $linkId = $row[0];

        // Salvataggio dei dati
        $stmt = dbConnection::getIstance()->getDb()->prepare("INSERT INTO stats (ClickDate, FK_LinkId) VALUES(?, ?)");
        $stmt->bind_param('si', $date, $linkId);
        $stmt->execute();
        $stmt->close();

        header("Location: $row[1]"); //redirect all'url originale
    } else {
        echo "Questo shortcode non esiste.<br><br><a href='home.php'>Torna alla home</a>";
    }
?>