<?php

//require_once("../database/dbConnection.php");


class user
{
    public $nickname;
    private $password;
    public $userId;

    public function __construct($nickname, $password)
    {
        $this->nickname = $nickname;
        $this->password = $password;
    }

    public function createUser(){
        $result = dbConnection::getIstance()->getDb()->query("SELECT Nickname FROM users WHERE Nickname = '$this->nickname'");
        if($result->num_rows == 1) {
            echo "<script type='text/javascript'>alert('Username già in uso');</script>";
            die();
        }

        $stmt = dbConnection::getIstance()->getDb()->prepare("INSERT INTO users (Nickname, Password) VALUES(?, ?)");
        $stmt->bind_param('ss', $this->nickname, $this->password);
        $stmt->execute();
        $this->userId = dbConnection::getIstance()->getDb()->insert_id;
        $stmt->close();
    }

    public static function getUser($nickname, $password){
        $hashedPassword = hash('sha256', $password);
        $result = dbConnection::getIstance()->getDb()->query("SELECT UserId, Nickname, Password FROM users WHERE Nickname = '$nickname' AND Password = '$hashedPassword'");
        if($result->num_rows == 1) {
            $row = $result->fetch_array(2);
            $user = new user($row[1], $row[2]);
            $user->userId = $row[0];
            return $user;
        } else {
            echo "<script type='text/javascript'>alert('Username o password errati.');</script>";
            die();
        }

        // METODO CONTRO LA SQL INJECTION: NON FUNZIONANTE (ritorna 0 row nonostante la query sembra giusta
        /*$stmt = dbConnection::getIstance()->getDb()->prepare("SELECT `Nickname`,`Password` FROM users WHERE Nickname=? AND Password=?");
        $stmt->bind_param('ss', $nickname, $hashedPassword);
        $stmt->execute();

        if($stmt->num_rows > 0) {
            $stmt->bind_result($dbUsername, $dbHashedPassword);
            return new user($dbUsername, $dbHashedPassword);
        } else {
            echo "<script type='text/javascript'>alert('Username o password non corretti');</script>";
            die();
        }*/

    }
}